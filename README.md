# The most complete Hugo boilerplate

Includes:
- hugo basic website structure
- multilingual support
- sass pipeline
- js pipeline with npm packages
- useful seo functions to defer loading
- gitlab-ci template to build and publish the static website

## How to use

1. Create your Gitlab project
2. Configure CI variables: FTP_USER, FTP_PASS, FTP_HOST, BASE_URL (https://domain.com)
3. Uncomment line 68 and delete line 69 in `gitlab-ci.yml`
4. Install Hugo and npm
5. Clone the boilerplate and make your website
6. Push to master to publish to / or any other branch to review your changes on /review-`branch`