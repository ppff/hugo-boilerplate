var isEven = require('is-even');

function initIsEven() {
    var input = document.querySelector("#is-even-input");
    var output = document.querySelector("#is-even-output");
    input.addEventListener("input", function() {
        output.innerHTML = isEven(input.value);
    });
}

export { initIsEven };