import { deferImages, deferCSS, deferJS } from './tools/seo.js'
import { initIsEven } from './main/iseven.js'

// ASAP
initIsEven();

// Once page is loaded
window.addEventListener('load', () => {
    deferCSS();
    deferJS();
    deferImages();
});