/** 
 * Download images in the background and display them only once loaded.
 * 
 * To use it, change <img src=...> into <img defer-src=...>
 */ 
function deferImages() {
    var images = document.querySelectorAll("img");
    Array.prototype.forEach.call(images, function(image, index, array) {
        if (image.getAttribute('defer-src') != null) {
            // Download image in background
            var img = new Image();
            img.src = image.getAttribute('defer-src');

            img.addEventListener('load', (e) => {
                // Display only once download is over
                image.setAttribute('src', img.src);
                image.removeAttribute('defer-src');
            });
        }
    });
}

/** 
 * Enable css urls only once this function is called.
 * 
 * To use it, change <link href=...> into <link defer-href=...>
 */ 
function deferCSS() {
    var links = document.querySelectorAll("link[rel=stylesheet]");
    Array.prototype.forEach.call(links, function(link, index, array) {
        if (link.getAttribute('defer-href') != null) {
            link.setAttribute('href', link.getAttribute('defer-href'));
            link.removeAttribute('defer-href');
        }
    });
}

/** 
 * Enable js urls only once this function is called.
 * 
 * To use it, change <script src=...> into <script defer-src=...>
 */ 
function deferJS() {
    var links = document.querySelectorAll("script");
    Array.prototype.forEach.call(links, function(link, index, array) {
        if (link.getAttribute('defer-src') != null) {
            link.setAttribute('src', link.getAttribute('defer-src'));
            link.removeAttribute('defer-src');
        }
    });
}

export { deferImages, deferCSS, deferJS };